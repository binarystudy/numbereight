import { ApiPath, AuthApiPath } from '../api';

export const WHITE_ROUTES = [`${ApiPath.AUTH}${AuthApiPath.LOGIN}`];

export const ONLY_ADMIN_LIST = [ApiPath.USERS];
