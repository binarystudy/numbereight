import { config } from 'dotenv';

config();

const { APP_PORT, APP_URL, MONGO_URI, SECRET_KEY, SOCKET_PORT } = process.env;

const ENV = {
  APP: {
    API_PATH: '',
    URL: APP_URL,
    PORT: APP_PORT,
    SOCKET_PORT,
  },
  DB: {
    URI: MONGO_URI as string,
  },
  JWT: {
    SECRET: SECRET_KEY as string,
    EXPIRES_IN: '24h',
  },
};

export { ENV };
