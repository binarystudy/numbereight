export const ApiPath = {
  AUTH: '/auth',
  USERS: '/users',
  MESSAGES: '/messages',
};
