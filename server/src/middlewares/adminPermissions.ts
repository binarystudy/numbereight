import { Request, Response, NextFunction } from 'express';
import { IUser } from '../data';
import { ValidationError, checkAdminPath } from '../helpers';

export const adminPermissions =
  (onlyAdminList: Array<string> = []) =>
  (req: Request, _: Response, next: NextFunction) => {
    const isAdmin = (req?.user as IUser)?.isAdmin;
    const isRouteForAdmin = onlyAdminList.some(checkAdminPath(req.path));
    if (isRouteForAdmin && !isAdmin) {
      throw new ValidationError('access denied');
    }
    next();
  };
