import { Request, Response, NextFunction } from 'express';
import { jwt as jwtMiddleware } from '../middlewares';

const authorization =
  (routesWhiteList: Array<string> = []) =>
  (req: Request, res: Response, next: NextFunction) =>
    routesWhiteList.some(route => route === req.path) ? next() : jwtMiddleware(req, res, next);

export { authorization };
