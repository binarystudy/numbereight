import { UserModel, IUser } from '../data';
import { createToken, InternalError } from '../helpers';

export class Auth {
  user: UserModel;
  constructor({ user }: { user: UserModel }) {
    this.user = user;
  }

  async login(user: IUser) {
    try {
      return {
        token: createToken({ id: user._id }),
        user: await this.user.findById(user._id).select('-password'),
      };
    } catch (_) {
      throw new InternalError('something wrong try later');
    }
  }

  async getUser({ _id }: IUser) {
    try {
      return {
        user: await this.user.findById(_id).select('-password'),
      };
    } catch (_) {
      throw new InternalError('something wrong try later');
    }
  }
}

export type AuthType = InstanceType<typeof Auth>;
