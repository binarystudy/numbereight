import { MessageModel, IMessage } from '../data';
import { ValidationError, InternalError } from '../helpers';

const userField = '_id name avatar';

export class Message {
  message: MessageModel;
  constructor({ message }: { message: MessageModel }) {
    this.message = message;
  }

  async getAll() {
    return {
      messages: await this.message.find({}).populate('user', userField),
    };
  }

  async createMessage({ id, message }: { id: string; message: IMessage }) {
    const newMessage = new this.message({ ...message, user: id });
    await newMessage.save();
    return await this.message.findById(newMessage._id).populate('user', userField);
  }

  async updateMessage(id: string, message: IMessage) {
    const messageForUpdate = await this.message.findById(id);
    if (!messageForUpdate) {
      throw new ValidationError(`message with id: ${id} is not exist!`);
    }
    const updatedMessage = await this.message
      .findByIdAndUpdate(id, message, { new: true })
      .populate('user', userField);
    return updatedMessage;
  }

  async deleteMessage(id: string) {
    const message = await this.message.findById(id);
    if (!message) {
      throw new ValidationError(`message with id: ${id} is not exist!`);
    }
    try {
      await this.message.deleteOne({ _id: id });
      return {
        message: 'success',
      };
    } catch (_) {
      throw new InternalError('something wrong');
    }
  }
}

export type MessageType = InstanceType<typeof Message>;
