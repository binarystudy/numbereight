import { User, Message } from '../data';

import { Auth, AuthType } from './auth';
import { User as UserServices, UserType } from './user';
import { Message as MessageServices, MessageType } from './message';

const auth = new Auth({ user: User });

const user = new UserServices({ user: User, message: Message });

const message = new MessageServices({ message: Message });

export { auth, AuthType, user, UserType, message, MessageType };
