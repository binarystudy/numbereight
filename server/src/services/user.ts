import { UserModel, IUser, MessageModel } from '../data';
import { ValidationError, InternalError } from '../helpers';

export class User {
  user: UserModel;
  message: MessageModel;
  constructor({ user, message }: { user: UserModel; message: MessageModel }) {
    this.user = user;
    this.message = message;
  }

  async getAll() {
    return await this.user.find({});
  }

  async createUser(user: IUser) {
    const { name } = user;
    const candidate = await this.user.findOne({ name });
    if (candidate) {
      throw new ValidationError(`user with name ${name} already exist!`);
    }
    try {
      const newUser = new this.user(user);
      await newUser.save();
      return newUser;
    } catch (e) {
      if (e.code === 11000) {
        throw new ValidationError(`user with ${user?.email} already exist!`);
      }
      throw new InternalError('something wrong');
    }
  }

  async updateUser(id: string, user: IUser) {
    try {
      const userForUpdate = await this.user.findById(id);
      if (!userForUpdate) {
        throw new ValidationError(`user with id: ${id} is not exist!`);
      }
      const updatedUser = await this.user.findByIdAndUpdate(id, user, { new: true });
      return updatedUser;
    } catch (e) {
      if (e.code === 11000) {
        throw new ValidationError(`user with ${user?.email} or ${user?.name} already exist!`);
      }
      throw new InternalError('something wrong');
    }
  }

  async deleteUser(id: string) {
    const user = await this.user.findById(id);
    if (!user) {
      throw new ValidationError(`user with id: ${id} is not exist!`);
    }
    try {
      await this.user.deleteOne({ _id: id });
      await this.message.deleteMany({ userId: id });
      return {
        message: 'success',
      };
    } catch (_) {
      throw new InternalError('something wrong');
    }
  }
}

export type UserType = InstanceType<typeof User>;
