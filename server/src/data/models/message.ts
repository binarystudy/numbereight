import { Schema, model, Types, Model, Document } from 'mongoose';

export interface IMessage extends Document {
  text: string;
  createdAt: string;
  updatedAt: string;
  liked?: boolean;
  user: string;
}

const messageSchema = new Schema<IMessage>(
  {
    text: { type: String, required: true },
    liked: { type: Boolean, default: false },
    user: { type: Types.ObjectId, ref: 'User' },
  },
  { timestamps: true }
);

export const Message = model<IMessage>('Message', messageSchema);

export type MessageModel = Model<IMessage, {}, {}>;
