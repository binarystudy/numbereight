import fs from 'fs';
import express, { Router } from 'express';
import mongoose from 'mongoose';
import path from 'path';
import passport from 'passport';
import { ENV, WHITE_ROUTES, ONLY_ADMIN_LIST } from './common';
import { initApi } from './api';
import {
  authorization as authorizationMiddleware,
  errorHandler as errorHandlerMiddleware,
  adminPermissions as adminPermissionsMiddleware,
} from './middlewares';
import './config/passport';

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(passport.initialize());

app.use(
  ENV.APP.API_PATH,
  authorizationMiddleware(WHITE_ROUTES),
  adminPermissionsMiddleware(ONLY_ADMIN_LIST)
);

app.use(ENV.APP.API_PATH, initApi(Router));

const staticPath = path.resolve(`${__dirname}/../client/build`);
app.use(express.static(staticPath));

app.get('*', (_req, res) => {
  res.write(fs.readFileSync(`${__dirname}/../client/build/index.html`));
  res.end();
});

app.use(errorHandlerMiddleware);

try {
  mongoose.connect(ENV.DB.URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false,
  });
  app.listen(ENV.APP.PORT, () => {
    console.info(`Server listening on port ${ENV.APP.PORT}!`);
  });
} catch (e) {
  console.info(`server not started => ${e}`);
}
