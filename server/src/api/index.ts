import { Application, Router } from 'express';
import { ApiPath } from '../common';
import { auth, user, message } from '../services';
import { initAuth } from './auth.api';
import { initUser } from './user.api';
import { initMessage } from './message.api';

const initApi = (app: typeof Router) => {
  const router = app();
  router.use(ApiPath.AUTH, initAuth(app, { authService: auth }));
  router.use(ApiPath.USERS, initUser(app, { userServices: user }));
  router.use(ApiPath.MESSAGES, initMessage(app, { messageServices: message }));

  return router;
};

export { initApi };
