import { Router } from 'express';
import { AuthApiPath } from '../common';
import { AuthType } from '../services';
import { authentication as authenticationMiddleware } from '../middlewares';
import { IUser } from '../data';

const initAuth = (app: typeof Router, services: { authService: AuthType }) => {
  const { authService } = services;
  const router = app();
  router
    .post(AuthApiPath.LOGIN, authenticationMiddleware, (req, res, next) =>
      authService
        .login(req.user as IUser)
        .then(data => res.send(data))
        .catch(next)
    )
    .post(AuthApiPath.USER, (req, res, next) =>
      authService
        .getUser(req.user as IUser)
        .then(data => res.send(data))
        .catch(next)
    );

  return router;
};

export { initAuth };
