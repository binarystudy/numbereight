import { Router } from 'express';
import { MessagesApiPath } from '../common';
import { MessageType } from '../services';
import { IUser } from '../data';

const initMessage = (app: typeof Router, services: { messageServices: MessageType }) => {
  const { messageServices } = services;
  const router = app();
  router
    .get(MessagesApiPath.ROOT, (req, res, next) =>
      messageServices
        .getAll()
        .then(data => res.send(data))
        .catch(next)
    )
    .post(MessagesApiPath.ROOT, (req, res, next) =>
      messageServices
        .createMessage({ id: (req.user as IUser).id, message: req.body })
        .then(data => res.send(data))
        .catch(next)
    )
    .put(MessagesApiPath.$ID, (req, res, next) =>
      messageServices
        .updateMessage(req.params.id, req.body)
        .then(data => res.send(data))
        .catch(next)
    )
    .delete(MessagesApiPath.$ID, (req, res, next) =>
      messageServices
        .deleteMessage(req.params.id)
        .then(data => res.send(data))
        .catch(next)
    );

  return router;
};

export { initMessage };
