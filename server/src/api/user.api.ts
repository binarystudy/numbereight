import { Router } from 'express';
import { UsersApiPath } from '../common';
import { UserType } from '../services';
import { IUser } from '../data';

const initUser = (app: typeof Router, services: { userServices: UserType }) => {
  const { userServices } = services;
  const router = app();
  router
    .get(UsersApiPath.ROOT, (req, res, next) =>
      userServices
        .getAll()
        .then(data => res.send(data))
        .catch(next)
    )
    .post(UsersApiPath.ROOT, (req, res, next) =>
      userServices
        .createUser(req.body as IUser)
        .then(data => res.send(data))
        .catch(next)
    )
    .put(UsersApiPath.$USER, (req, res, next) =>
      userServices
        .updateUser(req.params.id, req.body)
        .then(data => res.send(data))
        .catch(next)
    )
    .delete(UsersApiPath.$USER, (req, res, next) =>
      userServices
        .deleteUser(req.params.id)
        .then(data => res.send(data))
        .catch(next)
    );

  return router;
};

export { initUser };
