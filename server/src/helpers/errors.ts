abstract class CustomError extends Error {
  abstract status: number;
  constructor(message: string) {
    super(message);
    this.name = this.constructor.name;
  }
}

export class ValidationError extends CustomError {
  status = 400;
}

export class InternalError extends CustomError {
  status = 500;
}
