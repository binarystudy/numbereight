export const checkAdminPath =
  (path: string) =>
  (route: string): boolean => {
    const testRoute = new RegExp(`^${route}`);
    return testRoute.test(path);
  };
