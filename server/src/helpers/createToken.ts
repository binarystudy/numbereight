import jwt from 'jsonwebtoken';
import { ENV } from '../common';

const createToken = (data: Record<string, string>) =>
  jwt.sign(data, ENV.JWT.SECRET, { expiresIn: ENV.JWT.EXPIRES_IN });

export { createToken };
