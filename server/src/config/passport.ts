import passport from 'passport';
import { Strategy as LocalStrategy } from 'passport-local';
import { Strategy as JwtStrategy, ExtractJwt } from 'passport-jwt';
import { ENV } from '../common';
import { User } from '../data';

const options = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: ENV.JWT.SECRET,
};

passport.use(
  'login',
  new LocalStrategy({ usernameField: 'name' }, async (name, password, done) => {
    try {
      const user = await User.findOne({ name });
      if (!user) {
        return done({ status: 401, message: 'Incorrect name.' }, null);
      }

      return password === user.password
        ? done(null, user)
        : done({ status: 401, message: 'Password does not match.' }, null);
    } catch (err) {
      return done(err);
    }
  })
);

passport.use(
  new JwtStrategy(options, async ({ id }, done) => {
    try {
      const user = await User.findById(id);
      return user ? done(null, user) : done({ status: 401, message: 'Token is invalid.' }, null);
    } catch (err) {
      return done(err);
    }
  })
);
