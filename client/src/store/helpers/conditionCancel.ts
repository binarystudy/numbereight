export const conditionCancel =
  (type: string) =>
  (_: any, { getState }: { getState: () => unknown }) => {
    const state = getState() as any;
    if (state[type].loading) {
      return false;
    }
  };
