import { createAsyncThunk } from '@reduxjs/toolkit';
import { Message } from 'types';
import { notification } from 'helpers';
import { conditionCancel } from '../helpers';
import { ThunkApi } from './types';
import { ChatActionsTypes } from '../types';

export const createMessage = createAsyncThunk<Message, string, ThunkApi>(
  ChatActionsTypes.CREATE_MESSAGE,
  async (text, { extra, rejectWithValue }) => {
    try {
      const message: Message = await extra.services.messages.add({ text });
      notification.success('message created!');
      return message;
    } catch (e) {
      notification.error(e?.message || 'error');
      return rejectWithValue(e?.message || 'error');
    }
  },
  { condition: conditionCancel('chat') }
);

export const addMessages = createAsyncThunk<Message[], void, ThunkApi>(
  ChatActionsTypes.ADD_MESSAGES,
  async (_, { extra, rejectWithValue }) => {
    try {
      const { messages }: { messages: Message[] } = await extra.services.messages.getAll();
      return messages;
    } catch (e) {
      notification.error(e?.message || 'error');
      return rejectWithValue(e?.message || 'error');
    }
  }
);

export const deleteMessage = createAsyncThunk<string, string, ThunkApi>(
  ChatActionsTypes.DELETE_MESSAGE,
  async (id, { extra, rejectWithValue }) => {
    try {
      await extra.services.messages.delete(id);
      notification.success('message deleted!');
      return id;
    } catch (e) {
      notification.error(e?.message || 'error');
      return rejectWithValue(e?.message || 'error');
    }
  },
  { condition: conditionCancel('chat') }
);

export const updateMessage = createAsyncThunk<
  Message,
  { id: string; text?: string; liked?: boolean },
  ThunkApi
>(
  ChatActionsTypes.UPDATE_MESSAGE,
  async ({ id, text, liked }, { extra, rejectWithValue }) => {
    try {
      const message: Message = await extra.services.messages.update(id, { text, liked });
      notification.success('message updated!');
      return message;
    } catch (e) {
      notification.error(e?.message || 'error');
      return rejectWithValue(e?.message || 'error');
    }
  },
  { condition: conditionCancel('chat') }
);
