import { ServicesType } from 'services';

export interface ThunkApi {
  extra: {
    services: ServicesType;
  };
}
