import { createAsyncThunk, createAction } from '@reduxjs/toolkit';
import { User } from 'types';
import { notification } from 'helpers';
import { storage } from 'services';
import { ThunkApi } from './types';
import { ProfileActionTypes } from '../types';

interface ResponseData {
  user: User;
  token: string;
}

export const login = createAsyncThunk<ResponseData, Partial<User>, ThunkApi>(
  ProfileActionTypes.PROFILE_LOGIN,
  async (payload, { extra, rejectWithValue }) => {
    try {
      const response: ResponseData = await extra.services.auth.login(payload);
      storage.setItem(response.token);
      return response;
    } catch (e) {
      notification.error(e?.message || 'error');
      return rejectWithValue(e?.message || 'error');
    }
  }
);

export const getCurrentUser = createAsyncThunk<User, void, ThunkApi>(
  ProfileActionTypes.PROFILE_GET_CURRENT_USER,
  async (_, { extra, rejectWithValue }) => {
    try {
      const { user }: { user: User } = await extra.services.auth.getCurrentUser();
      return user;
    } catch (e) {
      notification.error(e?.message || 'error');
      return rejectWithValue(e?.message || 'error');
    }
  }
);

export const logout = createAction<void>(ProfileActionTypes.PROFILE_LOGOUT);
