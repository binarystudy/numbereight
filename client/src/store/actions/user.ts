import { createAsyncThunk } from '@reduxjs/toolkit';
import { conditionCancel } from 'store/helpers/conditionCancel';
import { User } from 'types';
import { notification } from 'helpers';
import { ThunkApi } from './types';
import { UserActionsTypes } from '../types';

export const createUser = createAsyncThunk<User, Partial<User>, ThunkApi>(
  UserActionsTypes.CREATE_USER,
  async (payload, { extra, rejectWithValue }) => {
    try {
      const user: User = await extra.services.users.add(payload);
      notification.success('user created!');
      return user;
    } catch (e) {
      notification.error(e?.message || 'error');
      return rejectWithValue(e?.message || 'error');
    }
  },
  { condition: conditionCancel('users') }
);

export const addUsers = createAsyncThunk<User[], void, ThunkApi>(
  UserActionsTypes.ADD_USERS,
  async (_, { extra, rejectWithValue }) => {
    try {
      const users: User[] = await extra.services.users.getAll();
      return users;
    } catch (e) {
      notification.error(e?.message || 'error');
      return rejectWithValue(e?.message || 'error');
    }
  }
);

export const deleteUser = createAsyncThunk<string, string, ThunkApi>(
  UserActionsTypes.DELETE_USER,
  async (id, { extra, rejectWithValue }) => {
    try {
      await extra.services.users.delete(id);
      notification.success('user deleted!');
      return id;
    } catch (e) {
      notification.error(e?.message || 'error');
      return rejectWithValue(e?.message || 'error');
    }
  },
  { condition: conditionCancel('users') }
);

export const updateUser = createAsyncThunk<User, { id: string; payload: Partial<User> }, ThunkApi>(
  UserActionsTypes.UPDATE_USER,
  async ({ id, payload }, { extra, rejectWithValue }) => {
    try {
      const user: User = await extra.services.users.update(id, payload);
      notification.success('user updated!');
      return user;
    } catch (e) {
      notification.error(e?.message || 'error');
      return rejectWithValue(e?.message || 'error');
    }
  },
  { condition: conditionCancel('users') }
);
