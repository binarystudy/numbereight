import { User } from 'types';

export interface ProfileState {
  user: User | null;
  loading: boolean;
}

export enum ProfileActionTypes {
  PROFILE_LOGIN = 'PROFILE_LOGIN',
  PROFILE_LOGOUT = 'PROFILE_LOGOUT',
  PROFILE_GET_CURRENT_USER = 'PROFILE_GET_CURRENT_USER',
}
