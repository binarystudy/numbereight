import { Message } from 'types';

export interface ChatState {
  messages: Message[];
  loading: boolean;
}

export enum ChatActionsTypes {
  MESSAGE_FOR_EDIT = 'MESSAGE_FOR_EDIT',
  CREATE_MESSAGE = 'CREATE_MESSAGE',
  DELETE_MESSAGE = 'DELETE_MESSAGE',
  ADD_MESSAGES = 'ADD_MESSAGES',
  UPDATE_MESSAGE = 'UPDATE_MESSAGE',
}
