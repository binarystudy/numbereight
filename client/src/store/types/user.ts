import { User } from 'types';

export interface UserState {
  users: User[];
  loading: boolean;
}

export enum UserActionsTypes {
  CREATE_USER = 'CREATE_USER',
  DELETE_USER = 'DELETE_USER',
  ADD_USERS = 'ADD_USERS',
  UPDATE_USER = 'UPDATE_USER',
}
