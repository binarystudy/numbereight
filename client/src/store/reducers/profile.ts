import { createSlice } from '@reduxjs/toolkit';
import { ProfileState } from '../types';
import { getCurrentUser, login, logout } from '../actions';

const initialState: ProfileState = {
  user: null,
  loading: true,
};

export const profile = createSlice({
  name: 'profile',
  initialState,
  reducers: {
    profileSetLoading(state, { payload }) {
      state.loading = payload;
    },
  },
  extraReducers: builder => {
    builder
      .addCase(login.fulfilled, (state, { payload }) => {
        state.user = payload.user;
        state.loading = false;
      })
      .addCase(login.pending, state => {
        state.loading = true;
      })
      .addCase(login.rejected, state => {
        state.loading = false;
      })
      .addCase(logout, state => {
        state.user = null;
      })
      .addCase(getCurrentUser.fulfilled, (state, { payload }) => {
        state.user = payload;
        state.loading = false;
      })
      .addCase(getCurrentUser.pending, state => {
        state.loading = true;
      })
      .addCase(getCurrentUser.rejected, state => {
        state.loading = false;
      });
  },
});

export const { profileSetLoading } = profile.actions;
