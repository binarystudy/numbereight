import { createSlice } from '@reduxjs/toolkit';
import { createMessage, deleteMessage, updateMessage, addMessages } from '../actions';
import { ChatState } from '../types';

const initialState: ChatState = {
  messages: [],
  loading: true,
};

export const chat = createSlice({
  name: 'chat',
  initialState,
  reducers: {},
  extraReducers: builder => {
    builder
      .addCase(createMessage.fulfilled, (state, { payload }) => {
        state.messages.push(payload);
        state.loading = false;
      })
      .addCase(createMessage.pending, (state, _) => {
        state.loading = true;
      })
      .addCase(createMessage.rejected, (state, { error }) => {
        state.loading = false;
      })
      .addCase(deleteMessage.fulfilled, (state, { payload }) => {
        state.messages = state.messages.filter(message => message._id !== payload);
        state.loading = false;
      })
      .addCase(deleteMessage.pending, (state, _) => {
        state.loading = true;
      })
      .addCase(deleteMessage.rejected, (state, { error }) => {
        state.loading = false;
      })
      .addCase(updateMessage.fulfilled, (state, { payload }) => {
        state.messages = state.messages.map(message =>
          message._id === payload._id ? payload : message
        );
        state.loading = false;
      })
      .addCase(updateMessage.pending, (state, _) => {
        state.loading = true;
      })
      .addCase(updateMessage.rejected, (state, { error }) => {
        state.loading = false;
      })
      .addCase(addMessages.fulfilled, (state, { payload }) => {
        state.loading = false;
        state.messages = payload;
      })
      .addCase(addMessages.pending, (state, _) => {
        state.loading = true;
      })
      .addCase(addMessages.rejected, (state, { error }) => {
        state.loading = false;
      });
  },
});
