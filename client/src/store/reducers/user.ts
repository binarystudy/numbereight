import { createSlice } from '@reduxjs/toolkit';
import { createUser, deleteUser, updateUser, addUsers } from '../actions';
import { UserState } from '../types';

const initialState: UserState = {
  users: [],
  loading: true,
};

export const user = createSlice({
  name: 'user',
  initialState,
  reducers: {},
  extraReducers: builder => {
    builder
      .addCase(createUser.fulfilled, (state, { payload }) => {
        state.users.push(payload);
        state.loading = false;
      })
      .addCase(createUser.pending, state => {
        state.loading = true;
      })
      .addCase(createUser.rejected, state => {
        state.loading = false;
      })
      .addCase(deleteUser.fulfilled, (state, { payload }) => {
        state.users = state.users.filter(user => user._id !== payload);
        state.loading = false;
      })
      .addCase(deleteUser.pending, state => {
        state.loading = true;
      })
      .addCase(deleteUser.rejected, state => {
        state.loading = false;
      })
      .addCase(updateUser.fulfilled, (state, { payload }) => {
        state.users = state.users.map(user => (user._id === payload._id ? payload : user));
        state.loading = false;
      })
      .addCase(updateUser.pending, state => {
        state.loading = true;
      })
      .addCase(updateUser.rejected, (state, { error }) => {
        state.loading = false;
      })
      .addCase(addUsers.fulfilled, (state, { payload }) => {
        state.users = payload;
        state.loading = false;
      })
      .addCase(addUsers.pending, state => {
        state.loading = true;
      })
      .addCase(addUsers.rejected, (state, { error }) => {
        state.loading = false;
      });
  },
});
