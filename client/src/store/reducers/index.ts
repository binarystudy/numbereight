import { combineReducers } from 'redux';
import { chat } from './chat';
import { user } from './user';
import { profile } from './profile';

export const rootReducer = combineReducers({
  chat: chat.reducer,
  users: user.reducer,
  profile: profile.reducer,
});

export { profileSetLoading } from './profile';
export type RootState = ReturnType<typeof rootReducer>;
