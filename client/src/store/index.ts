import { configureStore } from '@reduxjs/toolkit';
import { services } from 'services';
import { rootReducer } from './reducers';

export const store = configureStore({
  reducer: rootReducer,
  middleware: getDefaultMiddleware => {
    return getDefaultMiddleware({
      thunk: {
        extraArgument: {
          services,
        },
      },
    });
  },
});

export * from './reducers';
export * from './actions';
