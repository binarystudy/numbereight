export enum AppRoute {
  ROOT = '/',
  LOGIN = '/login',
  USERS = '/users',
  USER = '/users/:id',
  CHAT = '/chat',
  MESSAGE = '/chat/:id',
  ANY = '*',
}
