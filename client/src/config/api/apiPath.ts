export enum AuthPath {
  LOGIN = '/auth/login',
  USER = '/auth/user',
}

export enum UsersPath {
  ROOT = '/users',
}

export enum MessagesPath {
  ROOT = '/messages',
}
