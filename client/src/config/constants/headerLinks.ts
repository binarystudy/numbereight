import { AppRoute } from 'config/api';

export const HeaderLinks = [
  { link: AppRoute.USERS, category: 'isAdmin', label: 'Users', id: '1' },
  { link: AppRoute.CHAT, category: 'all', label: 'Chat', id: '2' },
];
