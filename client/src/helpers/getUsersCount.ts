import { Message } from '../types';

export const getUsersCount = (messages: Message[]) => {
  const users: Set<string> = new Set();
  messages.forEach(message => {
    users.add(message.user._id);
  });
  return users.size;
};
