export const changeClass = (
  element: HTMLElement,
  type: 'add' | 'remove',
  classes: string
): void => {
  const newClasses = classes.split(' ');
  newClasses.forEach((value) => element.classList[type](value));
};
