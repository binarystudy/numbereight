import { store } from 'react-notifications-component';

class Notification {
  private store = store;

  show(type: 'warning' | 'danger' | 'success', message: string, title: string) {
    this.store.addNotification({
      title: title,
      message: message,
      type: type,
      insert: 'top',
      container: 'top-right',
      animationIn: ['animate__animated', 'animate__fadeIn'],
      animationOut: ['animate__animated', 'animate__fadeOut'],
      dismiss: {
        duration: 5000,
        onScreen: true,
      },
    });
  }

  error(message: string): void {
    this.show('danger', message, 'Error!');
  }

  success(message: string): void {
    this.show('success', message, 'Success!');
  }
}

export const notification = new Notification();
