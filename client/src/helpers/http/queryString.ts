import { stringify } from 'query-string';

export const queryToString = (query: { [key: string]: string | string[] }) =>
  stringify(query, { arrayFormat: 'comma' });
