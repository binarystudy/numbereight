export const checkAdminPath = (routes: string[], path: string) => {
  const isAdminPath = routes.every(route => {
    const testRoute = new RegExp(`^${route}`);
    return testRoute.test(path);
  });
  return isAdminPath;
};
