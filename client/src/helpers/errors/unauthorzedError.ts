import { AppError } from './appError';

export class UnauthorizedError extends AppError {
  status = 401;
}
