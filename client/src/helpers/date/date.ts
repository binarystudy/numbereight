type F = (value: string) => string;

interface IDate {
  [key: string]: F;
}

interface Ioptions {
  year?: 'numeric';
  day?: '2-digit';
  month?: '2-digit' | 'long';
  hour?: '2-digit';
  minute?: '2-digit';
  weekday?: 'long';
  hour12?: boolean;
}

export const date = (locale: string = 'en-GB'): IDate => {
  const formatter = (date: string, options: Ioptions): string => {
    if (!date.length) {
      return '';
    }
    return new Intl.DateTimeFormat(locale, options).format(new Date(date));
  };

  const fullDate: F = (date) => {
    const options: Ioptions = {
      year: 'numeric',
      day: '2-digit',
      month: '2-digit',
      hour: '2-digit',
      minute: '2-digit',
      hour12: false,
    };
    const dateresult = formatter(date, options);
    return dateresult.replace(/\//g, '.').replace(',', '');
  };

  const dateDivider: F = (date) => {
    const options: Ioptions = {
      weekday: 'long',
      day: '2-digit',
      month: 'long',
    };
    return formatter(date, options);
  };

  const dateMessage: F = (date) => {
    const options: Ioptions = {
      hour: '2-digit',
      minute: '2-digit',
      hour12: false,
    };
    return formatter(date, options);
  };

  return { fullDate, dateMessage, dateDivider };
};

export const intlDate = date();
