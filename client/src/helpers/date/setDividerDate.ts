import { intlDate } from './date';

export const setDividerDate = (date: string) => {
  const dateCopy = new Date(date);
  if (dateCopy.toString() === 'Invalid Date') {
    return '';
  }
  const todayCopy = new Date();
  const today = new Date(todayCopy.getFullYear(), todayCopy.getMonth(), todayCopy.getDate());
  const yesterday = new Date(
    todayCopy.getFullYear(),
    todayCopy.getMonth(),
    todayCopy.getDate() - 1
  );
  if (yesterday > dateCopy) {
    return intlDate.dateDivider(date);
  }
  if (dateCopy >= today) {
    return 'Today';
  }
  return 'Yesterday';
};
