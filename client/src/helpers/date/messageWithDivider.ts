import { Message } from 'types';
import { setDividerDate } from './setDividerDate';

export const messageWithDivider = (messages: Message[]) => {
  const newMessages: Map<string, Message[]> = new Map();
  messages.forEach(message => {
    const dividerDate = setDividerDate(message.createdAt);
    newMessages.set(dividerDate, [...(newMessages.get(dividerDate) || []), message]);
  });
  return newMessages;
};
