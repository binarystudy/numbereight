export interface Message {
  _id: string;
  user: {
    _id: string;
    avatar: string;
    name: string;
  };
  text: string;
  createdAt: string;
  updatedAt: string;
  liked?: boolean;
}
