export type HttpQuery = { [key: string]: string | string[] };
