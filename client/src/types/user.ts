export interface User {
  name: string;
  email: string;
  avatar: string;
  isAdmin: boolean;
  password: string;
  _id: string;
}
