import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { useAppSelector } from 'hooks';
import { AppRoute } from 'config';
import { checkAdminPath } from 'helpers';

interface Props {
  exact?: boolean;
  path: string | string[];
  component: React.FC;
}

export const PrivateRoutes: React.FC<Props> = ({ component, ...rest }) => {
  const user = useAppSelector(state => state.profile.user);
  const hasUser = Boolean(user);
  const Component = component;
  return (
    <Route
      {...rest}
      render={props => {
        if (!hasUser) {
          return <Redirect to={{ pathname: AppRoute.LOGIN, state: { from: props.location } }} />;
        }
        const isAdminPath = checkAdminPath([AppRoute.USERS], props.location.pathname);
        if (!user?.isAdmin && isAdminPath) {
          return <Redirect to={{ pathname: AppRoute.CHAT, state: { from: props.location } }} />;
        }
        return <Component />;
      }}
    />
  );
};
