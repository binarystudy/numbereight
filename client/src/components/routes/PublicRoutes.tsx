import React from 'react';
import { Redirect, Route, useHistory } from 'react-router-dom';
import { useAppSelector } from 'hooks';
import { AppRoute } from 'config';
import { Login } from 'components/login';

interface Props {
  exact: boolean;
  path: string | string[];
}

interface History {
  location: Record<string, string>;
}

export const PublicRoutes: React.FC<Props> = props => {
  const user = useAppSelector(state => state.profile.user);
  const { location } = useHistory<History>();
  const hasUser = Boolean(user);
  if (hasUser) {
    const to = {
      pathname: user?.isAdmin ? AppRoute.USERS : AppRoute.CHAT,
      state: { from: location },
    };
    return <Redirect to={to} />;
  }
  return (
    <Route {...props}>
      <Login />
    </Route>
  );
};
