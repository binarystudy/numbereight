import React from 'react';
import './loader.scss';

export const Loader: React.FC = () => {
  return (
    <div className="wrapper-preloader">
      <div className="preloader"></div>
    </div>
  );
};
