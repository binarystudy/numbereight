import React, { useEffect, useState } from 'react';
import { fetchImg } from 'helpers';

import styles from './image.module.scss';

interface Props {
  src: string | null;
  alt: string;
}

export const Image: React.FC<Props> = props => {
  const { src, alt } = props;
  const [loading, setLoading] = useState<boolean>(true);
  const [isSrc, setIsSrc] = useState<boolean>(false);
  useEffect(() => {
    let mounted = true;
    const fetchI = async () => {
      if (src === null) {
        setLoading(false);
        return;
      }
      const loaded: boolean = await fetchImg(src);
      if (!mounted) {
        return;
      }
      setIsSrc(loaded);
      setLoading(false);
    };
    fetchI();
    return () => {
      mounted = false;
    };
  }, [src]);
  if (loading || !src || !src.length) {
    return <div className={`${styles.empty} ${styles.img}`}>{alt}</div>;
  }
  return <img className={styles.img} src={isSrc && src ? src : ''} alt={alt} />;
};
