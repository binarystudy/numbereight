import React from 'react';
import { Link } from 'react-router-dom';
import { useAppSelector } from 'hooks';
import { HeaderLinks } from 'config';

import './header.scss';

interface Props {
  onLogout: () => void;
}

export const Header: React.FC<Props> = ({ onLogout }) => {
  const { user } = useAppSelector(state => state.profile);
  if (!user) {
    return null;
  }
  return (
    <div className="header">
      <div className="header-content">
        <div className="header-logo">
          <img
            src="https://www.freepnglogos.com/uploads/nature-png/natural-health-logos-32.png"
            alt="logo"
          />
        </div>
        {HeaderLinks.filter(element => user.isAdmin || element.category !== 'isAdmin').map(
          element => (
            <Link key={element.id} to={element.link}>
              {element.label}
            </Link>
          )
        )}
      </div>
      <div className="header-userbar">
        <div className="header-name">{user.name}</div>
        <i className="fas fa-sign-out-alt fa-2x" onClick={() => onLogout()}></i>
      </div>
    </div>
  );
};
