import React from 'react';

interface Props {
  name: string;
  email: string;
  id: string;
  onEdit: (id: string) => void;
  onDelete: (id: string) => void;
}

export const UserItem: React.FC<Props> = props => {
  const { name, email, id, onDelete, onEdit } = props;
  return (
    <div className="user-item">
      <div className="user-info">
        <span className="user-name">{name}</span>
        <span className="user-email">{email}</span>
      </div>
      <div className="user-btn">
        <button className="btn btn-outline-primary" onClick={e => onEdit(id)}>
          Edit
        </button>
        <button className="btn btn-outline-dark" onClick={e => onDelete(id)}>
          Delete
        </button>
      </div>
    </div>
  );
};
