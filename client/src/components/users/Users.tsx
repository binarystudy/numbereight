import React, { useCallback } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { useAppSelector } from 'hooks';
import { deleteUser } from 'store';
import { UserAddKey } from 'config';
import { UserItem } from './userItem';

import './styles/users.scss';

export const Users: React.FC = () => {
  const { users } = useAppSelector(state => state.users);
  const dispatch = useDispatch();
  const history = useHistory();
  const location = useLocation();

  const handleEdit = useCallback(
    (id: string) => {
      history.push(`${location.pathname}/${id}`);
    },
    [history, location]
  );

  const handleDelete = useCallback(
    (id: string) => {
      dispatch(deleteUser(id));
    },
    [dispatch]
  );

  const handleAdd = () => {
    history.push(`${location.pathname}/${UserAddKey}`);
  };

  return (
    <div className="users-root">
      <div className="users-group">
        {users.map(user => {
          return (
            <UserItem
              key={user._id}
              id={user._id}
              name={user.name}
              email={user.email}
              onEdit={handleEdit}
              onDelete={handleDelete}
            />
          );
        })}
      </div>
      <div className="button-group">
        <button className="btn btn-success" onClick={handleAdd} style={{ margin: '5px' }}>
          Add user
        </button>
      </div>
    </div>
  );
};
