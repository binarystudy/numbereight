import React, { useEffect } from 'react';
import { useRouteMatch } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { AppRoute } from 'config';
import { addUsers } from 'store';
import { UserEdit } from './userEdit';
import { Users } from './Users';
import { useAppSelector } from 'hooks';
import { Loader } from 'components/common';

interface MatchParams {
  id: string;
}

export const UsersRoute: React.FC = () => {
  const { loading } = useAppSelector(state => state.users);
  const match = useRouteMatch<MatchParams>(AppRoute.USER);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(addUsers());
  }, [dispatch]);

  if (loading) {
    return <Loader />;
  }

  if (match) {
    return <UserEdit id={match?.params.id} />;
  }
  return <Users />;
};
