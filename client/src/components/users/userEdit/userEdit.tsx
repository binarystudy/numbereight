import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { createUser, updateUser } from 'store';
import { useAppSelector } from 'hooks';
import { User } from 'types';
import { inputs } from './data';
import { AppRoute, UserAddKey } from 'config';

import './userEdit.scss';

interface Props {
  id: string;
}

export const UserEdit: React.FC<Props> = ({ id }) => {
  const [data, setData] = useState<Record<string, string>>({ name: '', password: '', email: '' });
  const { users } = useAppSelector(state => state.users);
  const [user, setUser] = useState<User | null>(null);
  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    const userFilter = users.filter(user => user._id === id)[0];
    if (!userFilter) {
      return;
    }
    setUser(userFilter);
    if (!user) {
      return;
    }
    setData({
      name: user.name,
      password: user.password,
      email: user.email,
    });
  }, [id, users, user]);

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const notEmpty = Object.entries(data).every(([name, value]) => value.length);
    if (!notEmpty) {
      return;
    }
    dispatch(id === UserAddKey ? createUser(data) : updateUser({ id, payload: data }));
    history.push(AppRoute.USERS);
  };

  const handleChange = (event: React.FormEvent<HTMLInputElement>) => {
    const { name, value } = event.target as typeof event.target & {
      name: string;
      value: string;
    };
    setData(state => ({
      ...state,
      [name]: value.trim(),
    }));
  };

  const handleBack = () => {
    history.push(AppRoute.USERS);
  };

  return (
    <div className="edit-user-root">
      {user || id === UserAddKey ? (
        <div className="edit-user-content">
          <h3 className="edit-user-title">{id === UserAddKey ? 'New User' : 'Edit'}</h3>
          <form onSubmit={handleSubmit}>
            {inputs.map(input => (
              <div className={input.className} key={input.id}>
                <label htmlFor={input.name}>{input.label}</label>
                <input
                  type={input.type}
                  id={input.name}
                  value={data[input.name]}
                  name={input.name}
                  placeholder={input.placeHolder}
                  onChange={handleChange}
                />
              </div>
            ))}
            <div className="buttons">
              <input type="submit" className="btn btn-success" />
              <input
                type="button"
                className="btn btn-light"
                onClick={() => handleBack()}
                value="Back"
              />
            </div>
          </form>
        </div>
      ) : (
        <p className="message-wrong">User is wrong</p>
      )}
    </div>
  );
};
