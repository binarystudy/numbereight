export const inputs = [
  {
    label: 'Name',
    name: 'name',
    type: 'text',
    placeHolder: 'enter name',
    id: '1',
    className: 'form-group',
  },
  {
    label: 'Email',
    name: 'email',
    type: 'email',
    placeHolder: 'enter email',
    id: '2',
    className: 'form-group',
  },
  {
    label: 'Password',
    name: 'password',
    type: 'password',
    placeHolder: 'enter password',
    id: '3',
    className: 'form-group',
  },
];
