import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import { getCurrentUser, logout, profileSetLoading } from 'store';
import { useAppSelector } from 'hooks';
import { storage } from 'services';
import { AppRoute } from 'config';
import { Loader } from './common';
import { Header } from './header';
import { PrivateRoutes, PublicRoutes } from './routes';
import { ChatRoute } from './chat';
import { UsersRoute } from './users';
import { NotFoundPage } from './notFound';

export const App: React.FC = () => {
  const dispatch = useDispatch();
  const { profile } = useAppSelector(state => state);
  const hasUser = Boolean(profile.user);
  useEffect(() => {
    const token = storage.getItem();
    dispatch(token ? getCurrentUser() : profileSetLoading(false));
  }, [dispatch]);

  const handleLogout = () => {
    storage.removeItem();
    dispatch(logout());
  };

  if (profile.loading) {
    return <Loader />;
  }
  return (
    <Router>
      {hasUser && <Header onLogout={handleLogout} />}
      <Switch>
        <PublicRoutes exact path={[AppRoute.LOGIN]} />
        <PrivateRoutes path={AppRoute.USERS} component={UsersRoute} />
        <PrivateRoutes path={AppRoute.CHAT} component={ChatRoute} />
        <Route path={AppRoute.ROOT} exact>
          <Redirect to={hasUser ? AppRoute.CHAT : AppRoute.LOGIN} />
        </Route>
        <Route path={AppRoute.ANY} exact component={NotFoundPage} />
      </Switch>
    </Router>
  );
};
