import React from 'react';
import { intlDate } from 'helpers';

import './header.scss';

interface Props {
  chatName: string;
  usersCount: number;
  messagesCount: number;
  lastMessageDate: string;
}

export const Header: React.FC<Props> = props => {
  const { chatName, usersCount, messagesCount, lastMessageDate } = props;
  const { fullDate } = intlDate;
  const date = !lastMessageDate.length ? '' : fullDate(lastMessageDate);
  return (
    <div className="header-chat">
      <span className="header-chat-title">{chatName}:</span>
      <div>
        <span className="header-users-count">{usersCount}</span>
        <span> participants</span>
      </div>
      <div>
        <span className="header-messages-count">{messagesCount}</span>
        <span> messages</span>
      </div>
      <div>
        <span>last message at </span>
        <span className="header-last-message-date">{date}</span>
      </div>
    </div>
  );
};
