import React, { useState, useEffect } from 'react';

import './messageInput.scss';

interface Props {
  onSave: (text: string) => void;
  text?: string;
}

export const MessageInput: React.FC<Props> = props => {
  const { text = '', onSave } = props;
  const [disabled, setDisabled] = useState<boolean>(true);
  const [value, setValue] = useState<string>(text);
  useEffect(() => {
    setValue(text);
    setDisabled(true);
  }, [text]);
  const handleClick = () => {
    if (value === text || !value.length) {
      return;
    }
    onSave(value);
    setValue('');
    setDisabled(true);
  };
  const handleChange: React.ChangeEventHandler<HTMLTextAreaElement> = event => {
    const value = event.target.value.trim();
    setValue(value);
    setDisabled(value === text || !value.length);
  };
  return (
    <div className="message-input">
      <textarea value={value} name="message" onChange={handleChange} className="message-input-text">
        {value}
      </textarea>
      <input
        type="button"
        value="send"
        disabled={disabled}
        onClick={handleClick}
        className={`btn message-input-button${!disabled ? ' btn-success' : ''}`}
      />
    </div>
  );
};
