import React from 'react';
import { Divider } from '../divider/Divider';
import { OwnMessage } from '../message/OwnMessage';
import { Message } from '../message/Message';
import { Message as MessageType, User } from 'types';
import { messageWithDivider } from 'helpers';

interface Props {
  messages: MessageType[];
  user: User | null;
  onEdit: (id: string) => void;
  onDelete: (id: string) => void;
  onLiked: (id: string) => void;
}

export const MessageList: React.FC<Props> = props => {
  const { onDelete, onEdit, onLiked, messages, user } = props;
  const messagesMap = messageWithDivider(messages);
  if (!user) {
    return null;
  }

  return (
    <div className="message-list">
      {Array.from(messagesMap).map(([divider, messages], index) => {
        return (
          <div key={index + 1}>
            <Divider text={divider} />
            {messages.map(message => {
              return user._id === message.user._id ? (
                <OwnMessage
                  key={message._id}
                  message={message}
                  onDelete={() => onDelete(message._id)}
                  onEdit={() => onEdit(message._id)}
                />
              ) : (
                <Message key={message._id} message={message} onLiked={() => onLiked(message._id)} />
              );
            })}
          </div>
        );
      })}
    </div>
  );
};
