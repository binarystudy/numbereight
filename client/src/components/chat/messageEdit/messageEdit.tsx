import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { useAppSelector } from 'hooks';
import { AppRoute } from 'config';
import { Message } from 'types';
import { updateMessage } from 'store';

import './messageEdit.scss';

interface Props {
  id: string;
}

export const MessageEdit: React.FC<Props> = ({ id }) => {
  const { messages } = useAppSelector(state => state.chat);
  const [disabled, setDisabled] = useState<boolean>(true);
  const [message, setMessage] = useState<Message | null>(null);
  const [value, setValue] = useState<string>('');
  const history = useHistory();
  const dispatch = useDispatch();
  useEffect(() => {
    const messageFilter = messages.filter(message => message._id === id);
    if (!messageFilter.length) {
      return;
    }
    setMessage(messageFilter[0]);
    setValue(message?.text || '');
  }, [messages, id, message?.text]);

  const handleChange: React.ChangeEventHandler<HTMLTextAreaElement> = event => {
    const value = event.target.value.trim();
    setDisabled(value === message?.text || !value.length);
    setValue(value);
  };

  const handleSave = () => {
    if (!value || !value.length || !message) {
      return;
    }
    dispatch(
      updateMessage({
        text: value,
        id: message._id,
      })
    );
    history.push(AppRoute.CHAT);
  };

  const handleBack = () => {
    history.push(AppRoute.CHAT);
  };

  return (
    <div className="message-edit-root">
      {message ? (
        <div className="message-edit-content">
          <textarea value={value} onChange={handleChange} className="edit-message-input" />
          <div className="buttons">
            <button
              disabled={disabled}
              onClick={() => handleSave()}
              className={`btn edit-message-button${!disabled ? ' btn-success' : ''}`}
            >
              Save
            </button>
            <button className="btn edit-message-close" onClick={() => handleBack()}>
              Back
            </button>
          </div>
        </div>
      ) : (
        <p className="message-wrong">Message is wrong</p>
      )}
    </div>
  );
};
