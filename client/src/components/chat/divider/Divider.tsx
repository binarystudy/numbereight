import React from 'react';

import './divider.scss';

interface Props {
  text: string;
}

export const Divider: React.FC<Props> = ({ text }) => {
  return (
    <div className="divider">
      <div className="messages-divider">{text} </div>
    </div>
  );
};
