import React, { useEffect } from 'react';
import { useRouteMatch } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { AppRoute } from 'config';
import { addMessages } from 'store';
import { Loader } from 'components/common';
import { MessageEdit } from './messageEdit';
import { Chat } from './Chat';
import { useAppSelector } from 'hooks';

interface MatchParams {
  id: string;
}

export const ChatRoute: React.FC = () => {
  const { loading } = useAppSelector(state => state.chat);
  const match = useRouteMatch<MatchParams>(AppRoute.MESSAGE);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(addMessages());
  }, [dispatch]);

  if (loading) {
    return <Loader />;
  }

  if (match) {
    return <MessageEdit id={match?.params.id} />;
  }
  return <Chat />;
};
