import React from 'react';
import { Message as MessageType } from 'types';
import { intlDate } from 'helpers';
import { Image } from 'components/common/img/Image';

import './message.scss';

interface Props {
  message: MessageType;
  onLiked: () => void;
}

export const Message: React.FC<Props> = props => {
  const { message, onLiked } = props;
  const { dateMessage, fullDate } = intlDate;
  return (
    <div className="message">
      <div className="message-user-avatar">
        <Image src={message.user.avatar} alt={message.user.name} />
      </div>
      <div className="message-content">
        <div className="message-title">
          <div>
            <span className="message-user-name">{message.user.name}</span>
            <span className="message-time">{dateMessage(message.createdAt)}</span>
          </div>
          {message.updatedAt !== message.createdAt ? (
            <span className="message-edit-time">edit: {fullDate(message.updatedAt)}</span>
          ) : null}
        </div>
        <span className="message-text">{message.text}</span>
        <i
          className={`${message.liked ? 'message-liked fas' : 'message-like far'} fa-thumbs-up`}
          onClick={() => onLiked()}
        ></i>
      </div>
    </div>
  );
};
