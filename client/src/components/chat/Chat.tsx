import React, { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory, useLocation } from 'react-router-dom';
import { useAppSelector } from 'hooks';
import { getUsersCount } from 'helpers';
import { createMessage, updateMessage, deleteMessage } from 'store';
import { Header } from './header';
import { MessageList } from './list';
import { MessageInput } from './messageInput';

import './styles/chat.scss';

export const Chat: React.FC = () => {
  const history = useHistory();
  const location = useLocation();
  const { messages, user } = useAppSelector(state => ({
    messages: state.chat.messages,
    loading: state.chat.loading,
    user: state.profile.user,
  }));
  const dispatch = useDispatch();

  const handleDelete = useCallback(
    (id: string) => {
      dispatch(deleteMessage(id));
    },
    [dispatch]
  );

  const handleEdit = useCallback(
    (id: string) => {
      history.push(`${location.pathname}/${id}`);
    },
    [history, location]
  );

  const handleLiked = useCallback(
    (id: string) => {
      const likedMessage = messages.filter(message => message._id === id);
      if (!likedMessage.length) {
        return;
      }
      dispatch(updateMessage({ id, liked: !likedMessage[0].liked }));
    },
    [dispatch, messages]
  );

  const handleCreate = useCallback(
    (text: string) => {
      if (!text || !text.length) {
        return;
      }
      dispatch(createMessage(text));
    },
    [dispatch]
  );

  return (
    <div className="chat">
      <Header
        chatName="MyChat"
        messagesCount={messages.length}
        usersCount={getUsersCount(messages)}
        lastMessageDate={messages.length ? messages[messages.length - 1].createdAt : ''}
      />
      <MessageList
        messages={messages}
        onDelete={handleDelete}
        onEdit={handleEdit}
        onLiked={handleLiked}
        user={user}
      />
      <MessageInput onSave={handleCreate} />
    </div>
  );
};
