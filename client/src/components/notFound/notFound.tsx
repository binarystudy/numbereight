import React from 'react';
import { Link } from 'react-router-dom';
import { AppRoute } from 'config';

import styles from './notFound.module.scss';

export const NotFoundPage: React.FC = () => (
  <h2 className={styles.title}>
    <span className={styles.code}>404 Not Found</span>
    Go to
    <Link to={AppRoute.CHAT}> Chat </Link>
    page
  </h2>
);
