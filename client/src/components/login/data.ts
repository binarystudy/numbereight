export const inputs = [
  {
    label: 'Name',
    name: 'name',
    type: 'text',
    placeHolder: 'enter name',
    id: '1',
    className: 'form-group',
  },
  {
    label: 'Password',
    name: 'password',
    type: 'password',
    placeHolder: 'enter password',
    id: '2',
    className: 'form-group',
  },
];
