import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { login } from 'store';
import { inputs } from './data';

import './login.scss';

export const Login: React.FC = () => {
  const [data, setData] = useState<Record<string, string>>({ name: '', password: '' });
  const dispatch = useDispatch();
  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const notEmpty = Object.entries(data).every(([name, value]) => value.length);
    if (!notEmpty) {
      return;
    }
    dispatch(login(data));
  };
  const handleChange = (event: React.FormEvent<HTMLInputElement>) => {
    const { name, value } = event.target as typeof event.target & {
      name: string;
      value: string;
    };
    setData(state => ({
      ...state,
      [name]: value.trim(),
    }));
  };
  return (
    <div className="login-root">
      <div className="login">
        <h5 className="login-title">Login</h5>
        <form onSubmit={handleSubmit}>
          {inputs.map(input => (
            <div className={input.className} key={input.id}>
              <label htmlFor={input.name}>{input.label}</label>
              <input
                type={input.type}
                id={input.name}
                name={input.name}
                placeholder={input.placeHolder}
                onChange={handleChange}
              />
            </div>
          ))}
          <input type="submit" className="btn btn-success" />
        </form>
      </div>
    </div>
  );
};
