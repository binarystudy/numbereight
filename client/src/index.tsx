import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import ReactNotification from 'react-notifications-component';
import { store } from 'store';
import { App } from './components/App';

import 'styles/index.scss';
import 'react-notifications-component/dist/theme.css';

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <ReactNotification />
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);
