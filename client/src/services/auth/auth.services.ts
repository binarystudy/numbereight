import { AuthPath, HttpMethods, ContentType } from 'config';
import { User } from 'types';
import { HttpType } from '../http';

interface ILoad {
  path: string;
  payload?: Record<string, unknown> | string | undefined;
  hasAuth?: boolean;
}

export class Auth {
  private http: HttpType;
  constructor({ http }: { http: HttpType }) {
    this.http = http;
  }

  private load({ path, payload, hasAuth = false }: ILoad) {
    return this.http.load(path, {
      method: HttpMethods.POST,
      contentType: ContentType.JSON,
      hasAuth: hasAuth,
      payload: JSON.stringify(payload),
    });
  }

  login(payload: Partial<User>) {
    return this.load({ path: AuthPath.LOGIN, payload });
  }

  getCurrentUser() {
    return this.load({ path: AuthPath.USER, hasAuth: true });
  }
}
