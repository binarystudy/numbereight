import { HttpMethods, ContentType } from 'config';
import { HttpType } from '../http';

export abstract class Services {
  abstract servicePath: string;
  private http: HttpType;
  constructor({ http }: { http: HttpType }) {
    this.http = http;
  }

  getAll() {
    return this.http.load(this.servicePath, {
      method: HttpMethods.GET,
    });
  }

  update<T>(id: string, payload: Partial<T>) {
    return this.http.load(`${this.servicePath}/${id}`, {
      method: HttpMethods.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload),
    });
  }

  add<T>(payload: Partial<T>) {
    return this.http.load(this.servicePath, {
      method: HttpMethods.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload),
    });
  }

  delete(id: string) {
    return this.http.load(`${this.servicePath}/${id}`, {
      method: HttpMethods.DELETE,
    });
  }
}
