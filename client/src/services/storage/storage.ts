type TStorage = typeof localStorage | typeof sessionStorage;

export class Storage {
  private name: string;
  private storage: TStorage;
  constructor({ storage, name }: { storage: TStorage; name: string }) {
    this.storage = storage;
    this.name = name;
  }

  getItem() {
    const value = this.storage.getItem(this.name);
    return !value || !value.length ? null : JSON.parse(value);
  }

  setItem(value: unknown) {
    const valueJson = JSON.stringify(value);
    return this.storage.setItem(this.name, valueJson);
  }

  removeItem() {
    return this.storage.removeItem(this.name);
  }

  clear() {
    return this.storage.clear();
  }
}

export type StorageType = InstanceType<typeof Storage>;
