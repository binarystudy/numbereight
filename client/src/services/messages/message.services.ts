import { MessagesPath } from 'config';
import { Services } from '../abstractServices';

export class Messages extends Services {
  servicePath = MessagesPath.ROOT;
}
