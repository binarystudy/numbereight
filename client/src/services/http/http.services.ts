import { queryToString, FetchError, UnauthorizedError } from 'helpers';
import { HttpMethods, HttpHeader } from 'config';
import { HttpQuery } from 'types';
import { StorageType } from '../storage';

interface IOptions {
  contentType?: string;
  method?: string;
  query?: HttpQuery;
  payload?: BodyInit;
  hasAuth?: boolean;
}

export class Http {
  storage: StorageType;
  constructor({ storage }: { storage: StorageType }) {
    this.storage = storage;
  }

  load(url: string, options: IOptions) {
    const {
      method = HttpMethods.GET,
      payload = null,
      hasAuth = true,
      contentType,
      query,
    } = options;
    const headers = this.getHeaders({
      hasAuth,
      contentType,
    });

    return fetch(this.getUrl(url, query), {
      method,
      headers,
      body: payload,
    })
      .then(this.checkStatus)
      .then(this.getJson)
      .catch(this.error);
  }

  private getHeaders({ hasAuth, contentType }: { hasAuth?: boolean; contentType?: string }) {
    const headers = new Headers();

    if (contentType) {
      headers.append(HttpHeader.CONTENT_TYPE, contentType);
    }

    if (hasAuth) {
      const token = this.storage.getItem();
      headers.append(HttpHeader.AUTHORIZATION, `Bearer ${token}`);
    }

    return headers;
  }

  private getUrl(url: string, query?: HttpQuery) {
    return `${url}${query ? `?${queryToString(query)}` : ''}`;
  }

  private async checkStatus(response: Response) {
    if (response.ok) {
      return response;
    }
    let textBody;
    try {
      textBody = await response.text();
      const parsedException = JSON.parse(textBody);
      throw new Error(parsedException?.message ?? response.statusText);
    } catch (e) {
      if (textBody === 'Unauthorized') {
        throw new UnauthorizedError('Unauthorized');
      }
      throw new FetchError(e?.message || 'error');
    }
  }

  private getJson(response: Response) {
    return response.json();
  }

  private error(err: Error) {
    throw err;
  }
}

export type HttpType = InstanceType<typeof Http>;
