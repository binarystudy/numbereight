import { UsersPath } from 'config';
import { Services } from 'services/abstractServices';

export class Users extends Services {
  servicePath = UsersPath.ROOT;
}
