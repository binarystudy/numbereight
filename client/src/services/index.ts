import { StorageKey } from 'config';
import { Storage } from './storage';
import { Http } from './http';
import { Auth } from './auth';
import { Users } from './users';
import { Messages } from './messages';

export const storage = new Storage({ storage: localStorage, name: StorageKey.TOKEN });

const http = new Http({ storage });

const auth = new Auth({ http });

const users = new Users({ http });

const messages = new Messages({ http });

export const services = { auth, users, messages };

export type ServicesType = typeof services;
